import dynamic from 'next/dynamic'
import {Box, Container} from '@chakra-ui/react'
import Lynkco09Loader from '../lynkco09-loader'
import Footer from '../footer'

// const VConsole = dynamic(() => import('../vconsole'), {
//   ssr: false
// })

const Main = ({children}) => {
  return (
    <Box as="main" pb={8}>
      {/*<Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Oreo Li - Homepage</title>
      </Head>

      <NavBar path={router.asPath} />*/}

      <Container maxW="container.md" pt={14}>
        <LazyCar/>

        {children}

        <Footer/>
      </Container>

      {/* <VConsole /> */}
    </Box>
  )
}

const LazyCar = dynamic(() => import('../lynkco09'), {
  ssr: false,
  loading: () => <Lynkco09Loader/>
})

export default Main
